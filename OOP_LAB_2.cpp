#include <iostream>
#include <vector>
#include <math.h>
#include <time.h>

# define M_PI 3.14159265358979323846

class DoubleSubscript {
private:
    std::vector<std::vector<double>>& values;
    int j;
public:
    DoubleSubscript(std::vector<std::vector <double>>& _values, int _j) : values(_values) {
        j = _j;
    }

    double& operator[](int i) {
        return values[j][i];
    }
};

class Matrix {
public:
    Matrix() {
        this->rows = 0;
        this->cols = 0;
    }
    Matrix(int cols, int rows) {
        this->cols = cols;
        this->rows = rows;
        fill(cols, rows);
    }

    //Copy constructor
    Matrix(Matrix& m) {
        cols = m.cols;
        rows = m.rows;
        matrix = {};
        for (int i = 0;i < cols;i++) {
            std::vector<double> v;
            for (int j=0;j < rows;j++) {
                v.push_back(m[i][j]);
            }
            matrix.push_back(v);
        }
    }

    DoubleSubscript operator[](int j) {
        DoubleSubscript secondSubscript(matrix, j);
        return secondSubscript;
    }

    friend std::istream& operator >> (std::istream& input, Matrix& obj);
    friend std::ostream& operator << (std::ostream& output, Matrix& obj);
    friend bool operator == (Matrix& self, Matrix& other);
    
    Matrix& operator - (Matrix &other) {
        if (this->cols != other.cols || this->rows != other.rows) {
            throw std::invalid_argument("Matrixes of different shapes");
        }

        for (int i = 0;i < this->cols;i++) {
            for (int j = 0;j < this->rows;j++) {
                this->matrix[i][j] -= other.matrix[i][j];
            }
        }
    }

    Matrix& operator -= (Matrix& other) {
        return this->operator-(other);
    }

    Matrix& operator + (Matrix& other) {
        if (this->cols != other.cols || this->rows != other.rows) {
            throw std::invalid_argument("Matrixes of different shapes");
        }
        for (int i = 0;i < this->cols;i++) {
            for (int j = 0;j < this->rows;j++) {
                this->matrix[i][j] += other.matrix[i][j];
            }
        }
        return *this;
    }

    Matrix& operator += (Matrix& other) {
        return this->operator+(other);
    }

    Matrix& operator * (Matrix& other) {
        if (this->cols != other.cols || this->rows != other.rows) {
            throw std::invalid_argument("Matrixes of different shapes");
        }

        for (int i = 0;i < this->cols;i++) {
            for (int j = 0;j < this->rows;j++) {
                this->matrix[i][j] *= other.matrix[i][j];
            }
        }
    }

    Matrix& operator *= (Matrix& other) {
        return this->operator*(other);
    }
    
    void fill(int c, int r) {
        for (int i = 0;i < cols;i++) {
            std::vector<double> r;
            for (int j = 0;j < rows;j++) {
                r.push_back(0);
            }
            matrix.push_back(r);
        }
    }

    Matrix& operator - (double val) {
        for (int i = 0;i < this->cols;i++) {
            for (int j = 0;j < this->rows;j++) {
                this->matrix[i][j] -= val;
            }
        }
    }

    Matrix& operator -= (double val) {
        return this->operator-(val);
    }

    Matrix& operator + (double val) {
        for (int i = 0;i < this->cols;i++) {
            for (int j = 0;j < this->rows;j++) {
                this->matrix[i][j] += val;
            }
        }
        return *this;
    }

    Matrix& operator += (double val) {
        return this->operator+(val);
    }

    Matrix& operator * (double val) {
        for (int i = 0;i < this->cols;i++) {
            for (int j = 0;j < this->rows;j++) {
                this->matrix[i][j] *= val;
            }
        }
    }

    Matrix& operator *= (double val) {
        return this->operator*(val);
    }

    Matrix& operator ~ () {
        std::vector<std::vector<double>> transpose_matrix = {};
        for (int j = 0;j < this->rows;j++) {
            std::vector<double> v;
            for (int i = 0;i < this->cols; i++) {
                v.push_back(this->matrix[i][j]);
            }
            transpose_matrix.push_back(v);
        }
        this->matrix = transpose_matrix;
        int swap = this->cols;
        this->cols = this->rows;
        this->rows = swap;
        return *this;
    }

    std::vector<double> GausianSolver(std::vector<double> equal) {
        int cols = this->rows, rows = this->cols;
        if (equal.size() != rows) {
            throw std::invalid_argument("Incompatible size of equal vector");
        }

        double epsilon = 1e-8;
        int diag = std::min(rows, cols);

        for (int i = 0;i < diag;i++) {
            int col_idx = i;
            int row_idx = findNon0Row(i, diag, col_idx);
            if (row_idx == -1) {
                continue;
            }
            swapRows(i, row_idx, equal);
            makeZeroColumn(i, rows, cols, equal);
        }
       
        return extractResult(equal);
    }


    std::vector<double> extractResult(std::vector<double> res) {

        std::vector<double> answer = {};
        if (this->cols == this->rows) {
            for (int i = 0;i < res.size();i++) {
                answer.push_back(res[i] / matrix[i][i]);
            }
        }

        return answer;
    }

    void makeZeroColumn(int lead_row, int bottom, int right, std::vector<double> &eq) {
        double angle = matrix[lead_row][lead_row];
        for (int i = 0;i < bottom;i++) {
            if (i == lead_row) {
                continue;
            }
            double coef = matrix[i][lead_row] / angle;
            for (int j = lead_row;j < right;j++) {
                matrix[i][j] -= coef * matrix[lead_row][j];
            }
            eq[i] -= coef * eq[lead_row];
        }
    }

    void swapRows(int f, int s, std::vector<double> &res) {
        double swap_val = res[f];
        res[f] = res[s];
        res[s] = swap_val;

        std::vector<double> swap_row = matrix[f];
        matrix[f] = matrix[s];
        matrix[s] = swap_row;
    }

    int findNon0Row(int from, int to, int col) {
        int idx = -1;
        double epsilon = 1e-8;
        for (int i = from;i < to;i++) {
            if (abs(this->matrix[i][col]) > epsilon) {
                idx = i;
                break;
            }
        }
        return idx;
    }

    void generateLine(int x_count) {
        this->x_count = x_count;
        srand(time(nullptr));
        for (int i = 0;i < x_count;i++) {
            double x_val = (rand() % 100 / 100.0) * 60 - 30;
            x.push_back(x_val);
            std::cout << x_val << std::endl;
        }
    }

    std::vector<double> generatePoints(double left, double right, int count) {
        double step = (right - left) / count;
        int i = 0;
        while (i < count) {
            int y = 0;
            std::vector<double> vec;
            for (int i = 0;i < x_count;i++) {
                double dx = ((rand() % 100 / 100.0) * 5 - 10)/10;
                
                if (i == 0) {
                    vec.push_back(1);
                }
                else {
                    vec.push_back(left + dx);
                }

                y += vec[i] * x[i];
            }

            matrix.push_back(vec);
            double dy = rand() % 10 / 10.0 - 0.5;
            
            y += dy;

            predictions.push_back(y);
            
            left += step;
            i++;
        }
        this->rows = x_count;
        this->cols = count;
        return predictions;
    }

    std::vector<double> predictLine(std::vector<double> y) {
        if (y.size() != this->matrix.size()) {
            throw std::invalid_argument("PredictLine error");
        }
        
        Matrix tr(*this);
        tr = ~tr;
        Matrix left = *Matrix::matmul(tr, *this);
        std::vector <double> eq = Matrix::matmul(tr, y);

        return left.GausianSolver(eq);
    }

    static Matrix* matmul(Matrix& first, Matrix& other) {
        int a = first.matrix.size(), b = first.matrix[0].size(), 
            c = other.matrix.size(), d = other.matrix[0].size();
        if (b != c) {
            throw std::invalid_argument("Unappropriate dimensions");
        }

        Matrix mult(a, d);

        for (int i = 0;i < a;i++) {
            for (int j = 0;j < d;j++) {
                for (int k = 0;k < c;k++) {
                    mult[i][j] += first[i][k] * other[k][j];
                }
            }
        }
        return new Matrix(mult);
    }

    static std::vector<double> matmul(Matrix m, std::vector<double> vect) {
        int a = m.matrix.size(), b = m.matrix[0].size();
        if (b != vect.size()) {
            throw std::invalid_argument("matmul error wrong vector shape");
        }
        std::vector <double> mult;
        for (int i = 0;i < a;i++) {
            mult.push_back(0);

        }
        for (int i = 0;i < a;i++) {
            for (int j = 0;j < b;j++) {
                mult[i] = mult[i] + (m[i][j] * vect[j]);
            }
        }

        return mult;
    }

    void fillSymetrical() {
        if (cols != rows) {
            throw std::invalid_argument("Jakobi matrix can't be created");
        }
        srand(time(nullptr));
        for (int i = 0;i < cols;i++) {
            for (int j = i;j < rows;j++) {
                double r = rand() % 20 - 10.0;
                matrix[i][j] = matrix[j][i] = r;
            }
        }
    }

    std::vector<double> getEigenvalues() {
        if (cols != rows) {
            throw std::invalid_argument("Matrix must be of square shape");
        }
        int i, j;
        double max_val;
        do {
            i = j = -1;
            max_val = -100000000;
            findMaxElement(max_val, i, j);
            if (i < 0 || j < 0) {
                continue;
            }
            int f = std::min(i, j);
            int s = std::max(i, j);
            Matrix P = *createRotationMatrix(f, s);
            Matrix P_inv = *(P.inverseMatrix());
            *this = *Matrix::matmul(*Matrix::matmul(P, *this), P_inv);
        } while (i >= 0 && j >= 0);

        std::vector<double> ret;
        for (int i = 0;i < cols;i++) {
            ret.push_back(matrix[i][i]);
        }
        return ret;
    }

    Matrix* inverseMatrix() {
        Matrix inverse(cols, rows);
        std::vector<double> equal;
        for (int i = 0;i < cols;i++) {
            equal.push_back(0);
        }
        for (int i = 0;i < cols;i++) {
            Matrix c(*this);
            equal[i] = 1;
            std::vector<double> col = c.GausianSolver(equal);
            for (int j = 0;j < col.size();j++) {
                inverse[j][i] = col[j];
            }
            equal[i] = 0;
        }
        return new Matrix(inverse);
    }

    void findMaxElement(double &val, int &i, int &j) {
        double eps = 1e-4;
        for (int c = 0;c < cols;c++) {
            for (int r = c + 1;r < rows;r++) {
                if (matrix[c][r] > val && abs(matrix[c][r]) > eps) {
                    val = matrix[c][r];
                    i = c;
                    j = r;
                }
            }
        }
    }

    Matrix* createRotationMatrix(int f, int s) {
        Matrix p(cols, rows);
        double theta = 0.5 * atan(2 * matrix[f][s] / (matrix[f][f] - matrix[s][s]));
        double sn = sin(theta), cs = cos(theta);
        for (int i = 0;i < cols;i++) {
            p[i][i] = 1;
        }
        p[f][s] = sn;
        p[s][f] = -sn;
        p[f][f] = p[s][s] = cs;
        return new Matrix(p);
    }
    
    std::vector<std::vector<double>> getMatrix() {
        return this->matrix;
    }

    int getCols() {
        return this->cols;
    }

    int getRows() {
        return this->rows;
    }

    int setCols(int cols) {
        return this->cols = cols;
    }

    std::vector<double> getX() {
        return x;
    }

    int setRows(int rows) {
        return this->rows = rows;
    }

private:
    int rows, cols, x_count;
    std::vector<double> predictions;
    std::vector<double> x;
    std::vector<std::vector<double>> matrix;

};


int main()
{
    try {
        /*Matrix m;
        std::vector<double> y, res;
        m.generateLine(10);
        y = m.generatePoints(-100, 100, 1000);
        res = m.predictLine(y);
        std::cout << res.size() << std::endl;
        for (int i = 0;i < res.size();i++) {
            std::cout << res[i] << std::endl;
        }
        Matrix m(3, 3);
        m.fillSymetrical();
        std::cin >> m;
        std::cout << m;
        std::vector<double> r = m.getEigenvalues();
        for (int i = 0;i < r.size();i++) {
            std::cout << r[i] << " ";
        }*/
        Matrix m(3, 3);
        m.fillSymetrical();
        std::cout << m;
        std::vector<double> res = m.getEigenvalues();
        
        for (int i = 0;i < res.size();i++) {
            std::cout << res[i] << " ";
        }
        //m.getEigenvalues();
    }
    catch (std::invalid_argument& err) {
        std::cout << "Error occured \n" << err.what() << std::endl;
    }
    system("pause");
    return 0;
}

bool operator==(Matrix &self, Matrix& other) {
    if (self.getCols() != other.getCols() || self.getCols() != other.getRows()) {
        return false;
    }
    bool value = true;

    for (int i = 0;i < self.getCols();i++) {
        for (int j = 0;j < self.getRows();j++) {
            if (self[i][j] != other[i][j]) {
                value = false;
                break;
            }
        }
    }
    return value;
}

std::istream& operator >> (std::istream& input, Matrix& obj) {
    for (int i = 0;i < obj.cols;i++) {
        for (int j = 0;j < obj.rows;j++) {
            input >> obj.matrix[i][j];
        }
    }
    return input;
}

std::ostream& operator << (std::ostream& output, Matrix& obj) {
    for (int i = 0;i < obj.cols;i++) {
        for (int j = 0;j < obj.rows;j++) {
            output << obj.matrix[i][j] << " ";
        }
        std::cout << std::endl;
    }
    return output;
}
